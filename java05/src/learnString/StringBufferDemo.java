package learnString;

public class StringBufferDemo {

	public static void main(String[] args) {
		
		StringBufferDemo sbd = new StringBufferDemo();
		String[] str = {"a","b","c","d"};
		//sbd.StringMethod(str);	
		//sbd.StringBuffMethod(str);
		//sbd.StringbuffMethods();
		sbd.ternary();
	}
	private void ternary() {
		int no1 =30;
		int no2 = 20;
		
		int big = (no1>no2)?no1:no2;
		System.out.println(big);
		
		/*
		 * if(no1>no2) { System.out.println("no1 is big"); }else {
		 * System.out.println("no2 is big"); }
		 */
		
	}
	private void StringbuffMethods() {
		StringBuilder sb = new StringBuilder("selvakumar");
		sb.append("monish");
		sb.append("sarathkumar");
		System.out.println(sb);
		sb.insert(2, "123");
		System.out.println(sb);
		sb.replace(2, 5, "");
		System.out.println(sb);
		System.out.println(sb.reverse());
		
	}
	private void StringBuffMethod(String[] str) {
		StringBuffer s = new StringBuffer("");
		for(String ch:str) {
			System.out.println(s.hashCode());
			s=s.append(ch); //s.concat(ch) //""+a => a+b=>ab+c=abc+d=>abcd
		}
		System.out.println(s.hashCode());
		System.out.println(s);
	}

	private void StringMethod(String[] str) {
		String s = "";
		for(String ch:str) {
			System.out.println(s.hashCode());
			s=s.concat(ch); //s.concat(ch) //""+a => a+b=>ab+c=abc+d=>abcd
		}
		System.out.println(s.hashCode());
		System.out.println(s);
		
	}

}
