package learnException;

public class LowAttendanceException extends RuntimeException {

	public void allowExam(int percentage) {
		if(percentage>=75) {
			System.out.println("Allow for exam");
		}else {
			throw new LowAttendanceException();
		}
	}

}
