package learnFileIO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileDemo {

	public static void main(String[] args) {
		FileDemo fd = new FileDemo();
		//fd.day1();
		fd.day2();
		
		
	}

	private void day2()  {
		
		//list()
		/*
		 * String[] file_folder = note.list(); for(int i=0;i<file_folder.length;i++) {
		 * System.out.println(file_folder[i]); }
		 */
		//listFiles()
		/*
		 * File[] ff = note.listFiles(); for (int i = 0; i < ff.length; i++) { if
		 * (!ff[i].isDirectory()) //if(ff[i].isFile())
		 * System.out.println(ff[i].getName()); }
		 */
		File note = new 
				File("/home/vijay/Videos/BiriyaniShop1/rasika_shop/Mock1.txt");
		
		try {
			FileReader reader = new FileReader(note);
			int i = reader.read();
			while(i!=-1) {
			System.out.print((char)i);
				i=reader.read();
			}
			

			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}catch (IOException io) {
			io.printStackTrace();
		}
		
		
			
			
		
	}

	private void day1() {
		File note = new File("/home/vijay/Videos/BiriyaniShop1/rasika_shop"
				+ "/MenuItems.txt");
		//System.out.println(ff.mkdir());
		//System.out.println(ff.mkdirs());
		
		/*
		 * System.out.println(ff.canExecute()); System.out.println(ff.canRead());
		 * System.out.println(ff.canWrite()); System.out.println(ff.delete());
		 */
			System.out.println(note.exists());
		try {
			note.createNewFile();
			FileWriter pen = new FileWriter(note);
			//pen.append("idly");
			//pen.append("\npongal");
			//pen.close();
			BufferedWriter bw = new BufferedWriter(pen);
			bw.append("vijay");
			bw.newLine();
			bw.append("ajith");
			bw.newLine();
			bw.append("rajini");
			bw.flush();
			bw.close();
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
