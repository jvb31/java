package leranException;

public class LowAttendanceException extends RuntimeException {

	public void allow(int atten) {
		if (atten >= 75) {
			System.out.println("permit");
		} else {
			// LowAttendanceException lae = new LowAttendanceException();
			throw new LowAttendanceException();
		}
	}

	public String getMessage() {
		
		return "Low-attendance";
	}

}
