package leranException;

import java.util.Scanner;

public class ExceptionDemo {

	public static void main(String[] args) {
		ExceptionDemo ed = new ExceptionDemo();
		Scanner sc = new Scanner(System.in);
		System.out.println("enter 2 numbers");
		int no1 = sc.nextInt();
		int no2 = sc.nextInt();

		ed.divide(no1, no2);
		ed.add(no1, no2);

	}

	private void add(int no1, int no2) {
		System.out.println("add => "+(no1 + no2));
	}

	private void divide(int no1, int no2) {
		try {
			// Error possible area - block
			System.out.println("divide=> " + no1 / no2);
			int[] ar = new int[no1];
			System.out.println("length=> " + ar.length);
			for(int i=0;i<10;i++) {
				System.out.println(ar[i]);
			}
		} catch (ArithmeticException ae) {
			// Error handling area - block
			System.out.println("check no2");
		} catch (NegativeArraySizeException na) {
			System.out.println("check no1");
		}  catch(Exception e) {
			System.out.println("something wrong");
		} finally {
			//code cleaning area - block
			System.out.println("finally");
		}
	}

}
