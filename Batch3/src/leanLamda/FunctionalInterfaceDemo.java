package leanLamda;

@FunctionalInterface
public interface FunctionalInterfaceDemo {
	public static void main(String[] args) {
		FunctionalInterfaceDemo.display();
	}
	
	abstract int add(int no1,int no2);
	
	//abstract void add2();
	
	static void display() {
		System.out.println("display");
	}
	
	static void display2() {
		System.out.println("display");
	}
	
	default void test() {
		System.out.println("test");
	}
	
	default void test2() {
		System.out.println("test");
	}
	
	//1.only one abstract method allow
	
}
