package leanLamda;

import java.util.function.Function;
import java.util.function.Predicate;

public class LamdaDemo {

	public static void main(String[] args) 
	{	
		
		FunctionalInterfaceDemo fid = (no1, no2) -> {
			return no1 + no2;
		};
		System.out.println(fid);
		System.out.println(fid.add(30, 20));
		FunctionalInterfaceDemo.display();
		fid.test();
		 
		//int = Integer
		//float -Float
		//byte - Byte
		///double -Double
		Function<Integer, Integer> g = i->{return i;};
		
		System.out.println(g.apply(16));
		
		Predicate<Integer> h = (i)->{if(i<=10)
			return true;
		
		return false;
		};
		
		System.out.println(h.test(15));
	
	}
	   
	
	
	
	
	
}
