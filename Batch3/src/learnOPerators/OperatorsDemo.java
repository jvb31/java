package learnOPerators;

public class OperatorsDemo {

	public static void main(String[] args) {
		
		int no =10;
		//post unary operator
		System.out.println(no++);  //10 -> 11
		System.out.println(no); 
		
		//no = 11
		//post unary operator
		System.out.println(no--); //11 - 11-1=10
		System.out.println(no); //10
		
		//no = 10
		//pre-unary operator
		System.out.println(++no);//10+1 = 11 
		System.out.println(no);
		
		//no =11
		System.out.println(--no);//11-1 = 10
		
		System.out.println(3-2+2*2+3); //3-2+4+3
									   //1+4+3
									   //8
		//4/2+1-4*2+10  ==>19,11,15,15,7,19,5
		//2+1-4*2+10
		//2+1-8+10
		//3-8+10
		//-5+10
		//5
		System.out.println(4/2+1-4*2+10);
		
		int no1=10;
		int no2=10;	 //11
		System.out.println(no1<=no2 && ++no2<no1); // false
		System.out.println(no1>no2 || --no2<no1); // false
		System.out.println(no2); // 10
		 
		
		
		
	}

}
