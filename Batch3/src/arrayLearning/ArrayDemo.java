package arrayLearning;

public class ArrayDemo {

	public static void main(String[] args) {

		ArrayDemo ad = new ArrayDemo();
		//ad.type1();
		//ad.type2();
		//ad.reverse();
		//ad.sum();
		ad.operators();
	
	}


	private void operators() {
		int no1=90;
		int no2=89;
		
		int no3=70;
		int no4=65;
		//System.out.println(no3<no4);
		if(no1>no2 && no3>no4 ) {
			System.out.println("yes");
		}
		
	}


	private void sum() {
		int[] marks = { 97, 78, 98, 90, 79 };
					//  0    1   2   3   4	
		int total=0;
		int index=0;
		while(index<marks.length) {
			//System.out.println(marks[index]);
			total=total+marks[index];
			index++;
		//	System.out.println(total);
		}
		System.out.println(total);
		
	}


	private void reverse() {
		int[] marks = { 97, 78, 98, 90, 79 };
					//  0    1   2   3   4
		System.out.println(marks.length);
		
		for(int index =marks.length-1;index>=0;index--)
		{
		System.out.println(marks[index]);
		 //3
		
		}
		
	}

	private void type2() {
		int[] marks = new int[5];
		marks[0]=90;
		marks[1]=77;
		marks[2]=99;
		marks[3]=98;
		marks[4]=100;
		//System.out.println(marks[0]);
		
		for (int i = 0; i < marks.length; i++) {
			if (marks[i] % 2 != 0) {
				System.out.println(marks[i]);
			}
		}
		//for-each loop
		/*
		 * for (int mrk : marks) { if(mrk>90) System.out.println(mrk); }
		 */
		
	}

	private void type1() {
		int[] marks = { 97, 78, 98, 90, 79 };
		for (int index = 0; index < marks.length; index++) {
			System.out.println(marks[index]);
		}

	}

}
