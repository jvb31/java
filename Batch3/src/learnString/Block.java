package learnString;

public class Block {
	public Block() {
		System.out.println("constructor");
	}
	
	static{
		System.out.println("static-block");
	}
	
	{
		System.out.println("non-static-block");
	}
	public static void main(String[] args) {
		
		System.out.println("main-method");
		Block b = new Block();
	}

}
