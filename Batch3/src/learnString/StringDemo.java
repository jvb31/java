package learnString;

import java.util.Scanner;

public class StringDemo {

	public static void main(String[] args) {
		
		StringDemo sd = new StringDemo();
		Scanner sc = new Scanner(System.in);
		//sc.hashCode();
		String p1 = "DD";
		String p2 = "DD";
		String p3 = "DD";
		String p4 = "DD";
		String p5 = "DD";
		
		System.out.println(p1.hashCode());
		System.out.println(p2.hashCode());
		System.out.println(p3.hashCode());
		System.out.println(p4.hashCode());
		System.out.println(p5.hashCode());
		System.out.println("-------------------");
		p1 = "Dinos";
		p2="GN";
		
		System.out.println(p1.hashCode());
		System.out.println(p2.hashCode());
		System.out.println(p3.hashCode());
		System.out.println(p4.hashCode());
		System.out.println(p5.hashCode());
		
		Object ob = new Object();
		System.out.println(ob.hashCode());
		System.out.println(sd.hashCode());
		
	}
	public int hashCode() {
		return 65879;
	}

}
