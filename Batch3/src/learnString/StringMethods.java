package learnString;

import java.util.Date;

public class StringMethods {

	public static void main(String[] args) {
		
		String name1 = "SANTHOSH";
		String name2 = "santhosh";
		
		
		
		System.out.println(name1.length());
		for(int i=0;i<name1.length();i++) {
			if(name1.charAt(i)=='t') {
				System.out.println(i);
			}
		System.out.println(name1.charAt(i));
		}
		System.out.println(name1.startsWith("Sa"));
		System.out.println(name1.endsWith("h"));
		
		String date = "10 08 2023";
		
		
		String[] ar = date.split(" ");
		
		for (String str : ar) {
			System.out.println(str);
		}
		
		System.out.println(name1.compareTo(name2));
		System.out.println(name1.compareToIgnoreCase(name2));
		System.out.println(name1.concat(" K"));
		//name1 = name1.concat(" K");
		System.out.println(name1);
		//santhosh k
		System.out.println(name1.substring(4));
		System.out.println(name1.substring(2, 4));
		
		System.out.println(name1.contains("sn"));
		
		System.out.println(name1.equals(name2));
		
		System.out.println(name1.equalsIgnoreCase("santHosh"));
		
		String name3 = "";
		System.out.println(name3.isEmpty());
		//santhosh
		System.out.println(name1.indexOf('s'));
		System.out.println(name1.lastIndexOf('s'));
		
		char[] ch = name1.toCharArray();
		
		for(int i=0;i<ch.length;i++) {
			System.out.println(ch[i]);
		}
		
		System.out.println(name1.toUpperCase());
		
		System.out.println(name1.toLowerCase());
		
		String name  = "  rasi ka  ";
		System.out.println(name.length());
		name = name.trim();
		System.out.println(name.length());
		System.out.println(name);
	}

}
