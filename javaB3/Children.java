public class Children extends Parents
{

public static void main(String[] args)
{
Parents ch = new Parents(); //Dynamic binding or late binding
Children ch1 = new Children(); //static binding or early binding
ch.motivate();
ch.study();
ch1.play();
}
public void study()
{
System.out.println("study");	
}
public void play()
{
System.out.println("play");	
}

}