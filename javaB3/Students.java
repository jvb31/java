public class Students
{
	//global variable - non static
	String name;
	int rollNo;
//this is two parameter const	
public Students(String name,int rollNo)
{
	this.name = name;
	this.rollNo = rollNo;
}
public Students(Students stud1)
{
	this.name=stud1.name;
	this.rollNo=stud1.rollNo;
}
//this is main method	
public static void main(String[] args)
{
	
	Students stud1 = new Students("vignesh",101);
	Students stud2 = new Students(stud1);
	stud1.details();
	stud2.details();
}
//method definiton for printing student details	
public void details()
{
	System.out.println(name);
	System.out.println(rollNo);
}


}