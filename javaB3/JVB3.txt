String methods

name.compareTo()
name.compareToIgnoreCase()
name.concat()
name.contains()
name.endsWith()
name.equals()
name.equalsIgnoreCase()
name.hashCode()
name.isEmpty()
name.lastIndexOf()
name.startsWith()
name.substring()
name.toCharArray()
name.toLowerCase()
name.toLowerCase()
name.trim()
name.split()
---------------------------------------------------------

REGEX :
java.util.rejex - package

Pattern --> Compiled version of our regular expression
Matcher --> Finds the matching in the given input.
---------------------------------------------------------
String input = "My mobile number is 9884010000";
    Pattern patternObj = Pattern.compile("\\d{10}");
    Matcher matcherObj = patternObj.matcher(input);
    while(matcherObj.find())
    {
      System.out.println(matcherObj.group());
      System.out.println(matcherObj.start());
      System.out.println(matcherObj.end());

    }

Pattern patternObj = Pattern.compile("[0123456789]");

Pattern patternObj = Pattern.compile("[0-9]");

Pattern patternObj = Pattern.compile("[a-z]");

Pattern patternObj = Pattern.compile("[a-zA-Z]");

Pattern patternObj = Pattern.compile("[a-zA-Z0-9]");

Pattern patternObj = Pattern.compile("[^a-zA-Z0-9]");

Remove special char from given String
---------------------------------------------------------
String password = "Chennai is the capital of TamilNadu";
  Pattern patternObj = Pattern.compile("TamilNadu$");
    Matcher matcherObj = patternObj.matcher(password);
    while(matcherObj.find())
    {
      System.out.print(matcherObj.group());
    }

Pattern patternObj = Pattern.compile("^Chennai");
---------------------------------------------------------
String password = "Chennai is the capital city";
  Pattern patternObj = Pattern.compile("\\s");
    Matcher matcherObj = patternObj.matcher(password);
    int count = 0;
    while(matcherObj.find())
    {
      count++;
      System.out.print(matcherObj.group());
    }
    System.out.println(count);

Pattern patternObj = Pattern.compile("\\S");
Pattern patternObj = Pattern.compile("\\d");
Pattern patternObj = Pattern.compile("\\D");
-------------------------------------------------
String mobile = "9884012810";
  Pattern patternObj = Pattern.compile("[6-9][0-9]{9}");
    Matcher matcherObj = patternObj.matcher(mobile);
    while(matcherObj.find())
    {
      System.out.print(matcherObj.group());
    }
-------------------------------------------------------
String mobile = "9884012810";
Pattern patternObj = Pattern.compile("(0|91)?[0-9]{9}");
    Matcher matcherObj = patternObj.matcher(mobile);
    while(matcherObj.find())
    {
      System.out.print(matcherObj.group());
    }
---------------------------------------------------------
String pattern = "-";
       String input = "28-March-2023";
            Pattern patternObj = Pattern.compile(pattern);
            String[] items = patternObj.split(input);
            for(int i=0;i<items.length;i++)
            {
              System.out.println(items[i]);
            }


