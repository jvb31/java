public class Employee
{
String name;
int emp_id;	          
public Employee(String name,int emp_id) 
{
	//System.out.println("Are you two args constructor?");
	this.name=name; 
	this.emp_id=emp_id;
}	
public  Employee()
{
	//System.out.println("Are you zero args constructor?");
}	
public static void main(String[] args)
{
	Employee emp1 = new Employee("rasika",101);
	Employee emp2 = new Employee("sangeetha",102);
	Employee emp3 = new Employee();
	
	emp1.emp_details();
	emp2.emp_details();
	emp3.emp_details();
}
public  void emp_details()
{
	System.out.println(emp_id);
	System.out.println(name);	
}

}